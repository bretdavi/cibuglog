from seccomplite import Filter, ALLOW, KILL, MASKED_EQ, EQ, Arg
from ctypes import c_int
import mmap
import os


class LockDown:
    @classmethod
    def is_supported(cls):
        f = cls.minimal_filter()

        pid = os.fork()
        if pid == 0:
            f.load()     # pragma: no cover
            os._exit(0)  # pragma: no cover
        else:
            return os.waitpid(pid, 0)[1] == 0

    @classmethod
    def minimal_filter(cls):
        f = Filter(def_action=KILL)

        f.add_rule(ALLOW, "exit")
        f.add_rule(ALLOW, "exit_group")

        f.add_rule(ALLOW, "mmap", Arg(3, MASKED_EQ, mmap.MAP_ANONYMOUS, mmap.MAP_ANONYMOUS))
        f.add_rule(ALLOW, "mmap", Arg(4, EQ, c_int(-1).value))  # c_int() works around python representing as a float
        f.add_rule(ALLOW, "munmap")
        f.add_rule(ALLOW, "brk")

        return f

    def __init__(self):
        self.f = self.minimal_filter()

    def add_rule(self, *args, **kwargs):
        self.f.add_rule(*args, **kwargs)

    def start(self):
        self.f.load()
