from collections import namedtuple, OrderedDict, defaultdict

from django.template.loader import render_to_string
from django.utils.functional import cached_property
from django.utils.safestring import mark_safe

from . import models
from .templatetags.runconfig_diff import show_suppressed

import re


class ExecutionTime:
    def __init__(self, exec_time=None):
        self.minimum = exec_time
        self.maximum = exec_time
        self.count = 1 if exec_time is not None else 0

    @property
    def is_empty(self):
        return self.minimum is None or self.maximum is None or self.count == 0

    def __add__(self, b):
        if self.is_empty:
            return b
        elif b.is_empty:
            return self

        new = ExecutionTime()
        new.minimum = min(self.minimum, b.minimum)
        new.maximum = max(self.maximum, b.maximum)
        new.count = self.count + b.count

        return new

    def __eq__(self, b):
        return self.minimum == b.minimum and self.maximum == b.maximum and self.count == b.count

    def __round(self, value):
        if hasattr(value, 'total_seconds'):
            value = value.total_seconds()
        elif value is None:
            return "None"

        if int(value) == float(value):
            return str(value)
        else:
            return "{:.2f}".format(value)

    def __str__(self):
        minimum = self.__round(self.minimum)
        maximum = self.__round(self.maximum)

        if maximum != minimum:
            return "[{}, {}] s".format(minimum, maximum)
        else:
            return "[{}] s".format(minimum)


class RunConfigResultsForTest:
    def __init__(self, results):
        self.results = []

        if len(results) == 0:
            raise ValueError("No results provided")

        # Check that all results are from the same test
        first_result_test = results[0].test
        for result in results:
            if result.test != first_result_test:
                raise ValueError("Results from multiple tests")

            # Ignore statuses that are "not run"
            if result.status != result.status.testsuite.notrun_status:
                self.results.append(result)

        # Now that we checked all the results, make sure we have at least
        # one valid result in the set
        if len(self.results) == 0:
            raise ValueError("No results provided")

    @cached_property
    def statuses(self):
        statuses = dict()
        for result in self.results:
            if result.status not in statuses:
                statuses[result.status] = []
            statuses[result.status].append(result)

        return statuses

    @property
    def exec_time(self):
        return sum([ExecutionTime(r.duration) for r in self.results], ExecutionTime())

    @cached_property
    def __str(self):
        if len(self.results) == 1:
            return show_suppressed(self.results[0].status)
        else:
            # We have more than one result, which we means potentially diverging results. Check
            # if they are diverging by creating a dictionary containing all the statuses and
            # all their instances
            statuses = self.statuses

            if len(statuses) == 1:
                return "( {} {} )".format(len(self.results), show_suppressed(self.results[0].status))
            else:
                # We have more than one status, list them all
                status_results = []
                for status in sorted(statuses, key=lambda b: b.name):
                    status_results.append("{} {}".format(len(statuses[status]), show_suppressed(status)))
                return "( {} )".format(", ".join(status_results))

    def __markdown_single_result(self, result):
        return "[{}][R_{}]".format(show_suppressed(result.status), result.id)

    @cached_property
    def markdown(self):
        if len(self.results) == 1:
            return self.__markdown_single_result(self.results[0])
        else:
            results = [self.__markdown_single_result(r) for r in self.results]
            return "({})".format(", ".join(results))

    @property
    def was_run(self):
        return True

    @cached_property
    def failures(self):
        failures = []
        for result in self.results:
            if result.is_failure:
                failures.append(result)
        return failures

    @cached_property
    def is_failure(self):
        for result in self.results:
            if result.is_failure:
                return True
        return False

    @cached_property
    def is_suppressed(self):
        if len(self.failures) == 0:
            return False
        return all([not f.status.vetted for f in self.failures])

    @cached_property
    def associated_knownfailures(self):
        ret = list()
        for result in self.results:
            ret.extend(result.known_failures_cached)
        return ret

    @cached_property
    def all_failures_covered(self):
        # Find the list of failures in the list of results
        failures = set([r.id for r in self.results if r.is_failure])

        # Find the list of failures that have been associated to an issue
        known_failures = set([f.result_id for f in self.associated_knownfailures])

        # If the two sets are identical, then all failures are covered by at
        # least one known failure
        return failures == known_failures

    @cached_property
    def issues_covering(self):
        return set([f.matched_ifa.issue for f in self.associated_knownfailures])

    @cached_property
    def bugs_covering(self):
        bugs = set()
        for issue in self.issues_covering:
            for bug in issue.bugs_cached:
                bugs.add(bug)
        return bugs

    def __eq__(self, value):
        return (value is not None and
                self.statuses.keys() == value.statuses.keys() and
                self.bugs_covering == value.bugs_covering)

    def __str__(self):
        return self.__str


class RunConfigResultsForNotRunTest:
    def __init__(self):
        self.results = []

    @cached_property
    def statuses(self):
        return dict()

    @property
    def exec_time(self):
        return ExecutionTime()

    @property
    def __str(self):
        return "notrun"

    @property
    def was_run(self):
        return False

    @property
    def is_failure(self):
        return False

    @property
    def associated_knownfailures(self):
        return set()

    @property
    def all_failures_covered(self):
        return set()

    @property
    def issues_covering(self):
        return set()

    @property
    def bugs_covering(self):
        return set()

    def __eq__(self, value):
        return value is not None and str(self) == str(value)

    @property
    def markdown(self):
        return self.__str

    def __str__(self):
        return self.__str


class RunConfigResultsForTestDiff:
    # Flags
    FIX = 1
    REGRESSION = 2
    WARNING = 4
    SUPPRESSED = 8
    KNOWN_CHANGE = 16
    UNKNOWN_CHANGE = 32
    NEW_TEST = 64

    @property
    def is_fix(self):
        return (self.flags & self.FIX) > 0

    @property
    def is_regression(self):
        return (self.flags & self.REGRESSION) > 0

    @property
    def is_warning(self):
        return (self.flags & self.WARNING) > 0

    @property
    def is_supressed(self):
        return (self.flags & self.SUPPRESSED) > 0

    @property
    def is_known_change(self):
        return (self.flags & self.KNOWN_CHANGE) > 0

    @property
    def is_unknown_change(self):
        return (self.flags & self.UNKNOWN_CHANGE) > 0

    @property
    def is_new_test(self):
        return (self.flags & self.NEW_TEST) > 0

    @property
    def is_suppressed(self):
        return (not self.testsuite.vetted or not self.test.vetted or
                not self.machine.vetted or self.result_to.is_suppressed)

    def __init__(self, test, testsuite, machine, result_from, result_to,
                 collapsed_differences=[]):
        self.test = test
        self.testsuite = testsuite
        self.machine = machine
        self.result_from = result_from
        self.result_to = result_to
        self.collapsed_differences = collapsed_differences

        # Create the flags
        self.flags = 0
        if test.first_runconfig is None:
            self.flags |= self.NEW_TEST
        if result_from.is_failure and not result_to.is_failure:
            self.flags |= self.FIX | self.KNOWN_CHANGE
        else:
            if result_to.is_failure and result_to.all_failures_covered:
                self.flags |= self.KNOWN_CHANGE
            else:
                self.flags |= self.UNKNOWN_CHANGE

            if not self.flags & self.NEW_TEST and result_to.is_failure and self.is_suppressed:
                self.flags |= self.SUPPRESSED
            elif (not result_from.is_failure or self.flags & self.NEW_TEST) and result_to.is_failure:
                self.flags |= self.REGRESSION
            else:
                self.flags |= self.WARNING

    def __issues_to_str(self, results):
        if len(results.bugs_covering) == 0:
            return ""
        else:
            bugs = set()
            for bug in results.bugs_covering:
                bugs.add(bug.short_name)
            return " ([{}])".format("] / [".join(sorted(bugs)))

    def __diff_to_string(self, markdown=False):
        _from_issues = self.__issues_to_str(self.result_from)
        _to_issues = self.__issues_to_str(self.result_to)

        c_entries = ""
        if len(self.collapsed_differences) > 0:
            c_entries = " +{} similar issue{}".format(len(self.collapsed_differences),
                                                      "s" if len(self.collapsed_differences) > 1 else "")

        if markdown:
            result_from = self.result_from.markdown
            result_to = self.result_to.markdown
        else:
            result_from = str(self.result_from)
            result_to = str(self.result_to)

        ret = "{:<20}{}{} -> {}{}{}".format(show_suppressed(self.machine)+":",
                                            result_from.upper(), _from_issues,
                                            result_to.upper(), _to_issues,
                                            c_entries)
        if markdown:
            for r in self.result_from.results:
                ret += "\n   [R_{}]: {}".format(r.id, r.url)
            for r in self.result_to.results:
                ret += "\n   [R_{}]: {}".format(r.id, r.url)

        return mark_safe(ret)

    def __str__(self):
        return self.__diff_to_string()

    def markdown(self):
        return self.__diff_to_string(markdown=True)


class RunConfigResultsDiff:
    def __init__(self, results_diff_lst, no_compress=False):
        self._results = results_diff_lst
        self.no_compress = no_compress

    @cached_property
    def testsuites(self):
        testsuites = dict()
        for diff in self._results:
            if diff.testsuite not in testsuites:
                testsuites[diff.testsuite] = RunConfigResultsDiff([], no_compress=self.no_compress)
            testsuites[diff.testsuite]._results.append(diff)

        ret = OrderedDict()
        for testsuite in sorted(testsuites.keys(), key=lambda x: str(x.name)):
            ret[testsuite] = testsuites[testsuite]

        return ret

    @cached_property
    def tests(self):
        tests = dict()
        for diff in self._results:
            if diff.test not in tests:
                tests[diff.test] = RunConfigResultsDiff([], no_compress=self.no_compress)
            tests[diff.test]._results.append(diff)

        ret = OrderedDict()
        for test in sorted(tests.keys(), key=lambda x: str(x.name)):
            ret[test] = tests[test]

        return ret

    @cached_property
    def machines(self):
        machines = dict()
        for diff in self._results:
            if diff.machine not in machines:
                machines[diff.machine] = RunConfigResultsDiff([], no_compress=self.no_compress)
            machines[diff.machine]._results.append(diff)

        ret = OrderedDict()
        for machine in sorted(machines.keys(), key=lambda x: str(x.name)):
            ret[machine] = machines[machine]

        return ret

    @cached_property
    def to_statuses(self):
        statuses = defaultdict(int)
        for diff in self._results:
            for status, results in diff.result_to.statuses.items():
                statuses[status] += len(results)

        ret = OrderedDict()
        for status in sorted(statuses.keys(), key=lambda x: str(x.name)):
            ret[status] = statuses[status]

        return ret

    @cached_property
    def to_exec_times(self):
        return sum([diff.result_to.exec_time for diff in self._results], ExecutionTime())

    def filter(self, flags):
        rl = list()
        for r in self._results:
            if (r.flags & flags) == flags:
                rl.append(r)
        return rl

    @cached_property
    def new_changes(self):
        return RunConfigResultsDiff(self.filter(RunConfigResultsForTestDiff.UNKNOWN_CHANGE),
                                    no_compress=self.no_compress)

    @cached_property
    def known_changes(self):
        return RunConfigResultsDiff(self.filter(RunConfigResultsForTestDiff.KNOWN_CHANGE),
                                    no_compress=self.no_compress)

    @cached_property
    def fixes(self):
        return RunConfigResultsDiff(self.filter(RunConfigResultsForTestDiff.FIX),
                                    no_compress=self.no_compress)

    @cached_property
    def regressions(self):
        return RunConfigResultsDiff(self.filter(RunConfigResultsForTestDiff.REGRESSION),
                                    no_compress=self.no_compress)

    @cached_property
    def warnings(self):
        return RunConfigResultsDiff(self.filter(RunConfigResultsForTestDiff.WARNING),
                                    no_compress=self.no_compress)

    @cached_property
    def suppressed(self):
        return RunConfigResultsDiff(self.filter(RunConfigResultsForTestDiff.SUPPRESSED),
                                    no_compress=self.no_compress)

    @cached_property
    def new_tests(self):
        return RunConfigResultsDiff(self.filter(RunConfigResultsForTestDiff.NEW_TEST),
                                    no_compress=self.no_compress)

    @cached_property
    def compressed(self):
        if self.no_compress:
            return self

        bugs_machine = OrderedDict()
        for r in self._results:
            key = (r.machine, str(r.result_from), frozenset(r.result_from.bugs_covering),
                   str(r.result_to), frozenset(r.result_to.bugs_covering))

            if key not in bugs_machine:
                bugs_machine[key] = []
            bugs_machine[key].append(r)

        rl = list()
        for bm in bugs_machine:
            first = bugs_machine[bm][0]
            if len(bugs_machine[bm]) > 0:
                first = RunConfigResultsForTestDiff(first.test, first.testsuite, first.machine,
                                                    first.result_from, first.result_to,
                                                    bugs_machine[bm][1:])
            rl.append(first)

        return RunConfigResultsDiff(rl, no_compress=self.no_compress)

    def __len__(self):
        return len(self._results)

    def __iter__(self):
        return iter(self._results)


class RunConfigResults:
    def __init__(self, results):
        self.results = results

    @cached_property
    def keys(self):
        ResultKey = namedtuple('ResultKey', ['testsuite', 'test', 'machine'])

        keys = set()
        for ts in self.results:
            for test in self.results[ts]:
                for machine in self.results[ts][test]:
                    keys.add(ResultKey(ts, test, machine))
        return keys

    def __getitem__(self, key):
        if (key.testsuite in self.results and
           key.test in self.results[key.testsuite] and
           key.machine in self.results[key.testsuite][key.test]):
            try:
                return RunConfigResultsForTest(self.results[key.testsuite][key.test][key.machine])
            except ValueError:
                # The results were invalid, say we have no results
                return RunConfigResultsForNotRunTest()
        else:
            return RunConfigResultsForNotRunTest()


class RunConfigDiff:
    def __init__(self, runcfg_from, runcfg_to, max_missing_hosts=0.5, no_compress=False):
        self.runcfg_from = runcfg_from
        self.runcfg_to = runcfg_to
        self.max_missing_hosts = max_missing_hosts
        self.no_compress = no_compress

    def __import_runcfg_results(self, **filters):
        results = dict()

        # Organise the results like this: Testsuite --> Test --> Machine --> [results]
        failures = dict()
        query = models.TestResult.objects.filter(**filters).prefetch_related('test', 'test__first_runconfig',
                                                                             'ts_run', 'ts_run__machine',
                                                                             'ts_run__machine__aliases',
                                                                             'status__testsuite',
                                                                             'status').order_by('test__name')
        for result in query.defer('start', 'duration', 'command', 'stdout', 'stderr', 'dmesg'):
            ts = result.status.testsuite
            if ts not in results:
                results[ts] = OrderedDict()

            if result.test not in results[ts]:
                results[ts][result.test] = dict()

            machine = result.ts_run.machine
            if machine.aliases is not None:
                machine = machine.aliases

            if machine not in results[ts][result.test]:
                results[ts][result.test][machine] = []

            # Avoid generating a ton of requests for all the known failures by
            # setting all results' known failures to an empty list, recording
            # the list of failures, then later updating the results' known
            # failures with the result of one big query for all failures
            result.known_failures_cached = list()
            if result.is_failure:
                failures[result.id] = result

            results[ts][result.test][machine].append(result)

        # Now look for the known failures associated to the failures we found
        query = models.KnownFailure.objects.filter(result_id__in=failures.keys())
        for failure in query.prefetch_related('matched_ifa__issue__bugs__tracker'):
            failures[failure.result_id].known_failures_cached.append(failure)

        return results

    @cached_property
    def builds(self):
        ret = OrderedDict()

        # First, find the list of builds that differ
        diff = self.runcfg_from.builds_ids_cached.symmetric_difference(self.runcfg_to.builds_ids_cached)

        # Find from which components each differing build is from
        builds = dict()
        components = dict()
        for build in models.Build.objects.filter(id__in=diff).prefetch_related('component'):
            builds[build.id] = build

            if build.component not in components:
                components[build.component] = set([build.id])
            else:
                components[build.component].add(build.id)

        # Go through all the components and find from/to which build it goes
        BuildDiff = namedtuple('BuildDiff', ['from_build', 'to_build'])
        for component in sorted(components.keys(), key=lambda c: c.name):
            _from_ids = components[component] - self.runcfg_to.builds_ids_cached
            _to_ids = components[component] - self.runcfg_from.builds_ids_cached

            if len(_from_ids) > 1 or len(_to_ids) > 1:
                raise ValueError("One component has multiple builds in one runconfig")

            _from = builds[list(_from_ids)[0]] if len(_from_ids) == 1 else None
            _to = builds[list(_to_ids)[0]] if len(_to_ids) == 1 else None

            ret[component] = BuildDiff(_from, _to)

        return ret

    @cached_property
    def builds_all(self):
        ids = self.runcfg_from.builds_ids_cached | self.runcfg_to.builds_ids_cached
        return sorted(models.Build.objects.filter(id__in=ids), key=lambda x: x.name)

    @cached_property
    def runcfg_from_results(self):
        return self.__import_runcfg_results(ts_run__runconfig=self.runcfg_from)

    @cached_property
    def runcfg_to_results(self):
        return self.__import_runcfg_results(ts_run__runconfig=self.runcfg_to)

    @cached_property
    def results(self):
        diffs = list()

        ts_from = RunConfigResults(self.runcfg_from_results)
        ts_to = RunConfigResults(self.runcfg_to_results)

        # Go through all the result keys and store the differing results
        keys = ts_from.keys | ts_to.keys
        for key in keys:
            result_from = ts_from[key]
            result_to = ts_to[key]
            if result_to.was_run and result_from != result_to:
                # Do not create changes for NOTRUN -> PASS and NOTRUN -> KNOWN FAILURE
                if not result_from.was_run and (not result_to.is_failure or result_to.all_failures_covered):
                    continue

                diffs.append(RunConfigResultsForTestDiff(key.test, key.testsuite, key.machine, result_from, result_to))

        return RunConfigResultsDiff(diffs, no_compress=self.no_compress)

    @cached_property
    def new_tests(self):
        diffs = list()

        # no need to show anything if the to_runconfig is not temporary
        if not self.runcfg_to.temporary:
            return diffs

        ts_from = RunConfigResults(self.runcfg_from_results)
        ts_to = RunConfigResults(self.runcfg_to_results)

        for key in ts_to.keys:
            result_from = ts_from[key]
            result_to = ts_to[key]

            if key.test.first_runconfig is None:
                diffs.append(RunConfigResultsForTestDiff(key.test, key.testsuite, key.machine,
                                                         result_from, result_to))

        return RunConfigResultsDiff(diffs, no_compress=self.no_compress)

    @cached_property
    def has_suppressed_results(self):
        for result in self.results:
            if result.is_suppressed:
                return True
        return False

    @cached_property
    def bugs(self):
        bugs = set()
        for result in self.results:
            bugs.update(result.result_from.bugs_covering)
            bugs.update(result.result_to.bugs_covering)
        return sorted(bugs, key=lambda x: str(x.short_name))

    @cached_property
    def status(self):
        has_regressions = False
        has_warnings = False

        for result in self.results.new_changes:
            if not result.is_suppressed:
                has_regressions |= result.is_regression
                has_warnings |= result.is_warning

        if has_regressions or not self.has_sufficient_machines:
            return "FAILURE"
        elif has_warnings:
            return "WARNING"
        else:
            return "SUCCESS"

    @cached_property
    def testsuites(self):
        TestSuiteDiff = namedtuple('TestSuiteDiff', ['all', 'runcfg_from', 'runcfg_to', 'new', 'removed'])

        ts_from = self.runcfg_from_results.keys()
        ts_to = self.runcfg_to_results.keys()

        return TestSuiteDiff(all=sorted(ts_from | ts_to), runcfg_from=ts_from, runcfg_to=ts_to,
                             new=ts_to - ts_from, removed=ts_from - ts_to)

    def _get_machine_list(self, results):
        machines = set()
        for ts in results:
            for test in results[ts]:
                for machine in results[ts][test]:
                    machines.add(machine)
        return machines

    @cached_property
    def machines(self):
        MachineDiff = namedtuple('MachineDiff', ['all', 'runcfg_from', 'runcfg_to', 'new', 'removed'])

        machines_from = self._get_machine_list(self.runcfg_from_results)
        machines_to = self._get_machine_list(self.runcfg_to_results)

        return MachineDiff(all=machines_from | machines_to,
                           runcfg_from=machines_from, runcfg_to=machines_to,
                           new=machines_to - machines_from,
                           removed=machines_from - machines_to)

    @property
    def has_sufficient_machines(self):
        return len(self.machines.removed) <= self.max_missing_hosts * len(self.machines.runcfg_from)

    @cached_property
    def text(self):
        ret = render_to_string("CIResults/runconfigdiff.txt", {"diff": self})

        # Look for all the [R_\d+] patterns and replace the id with a smaller one
        matches = list(OrderedDict.fromkeys(re.findall(r'\[R_\d+\]', ret)))
        for i, m in enumerate(matches):
            ret = ret.replace(m, '[{}]'.format(i+1))

        return ret
