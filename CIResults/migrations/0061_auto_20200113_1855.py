# Generated by Django 2.2.5 on 2020-01-13 18:55

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CIResults', '0060_auto_20191213_1319'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bugtracker',
            name='features_field',
        ),
        migrations.RemoveField(
            model_name='bugtracker',
            name='platforms_field',
        ),
        migrations.AddField(
            model_name='bug',
            name='custom_fields',
            field=django.contrib.postgres.fields.jsonb.JSONField(default=dict, help_text='Mapping of customfields and values for the Bug. This field will be automatically populated based on BugTracker.custom_fields_map field'),
        ),
        migrations.AddField(
            model_name='bugtracker',
            name='custom_fields_map',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, help_text="Mapping of custom_fields that should be included when polling Bugs from this BugTracker, e.g. {'customfield_xxxx': 'severity', 'customfield_yyyy': 'build_number'}. If a customfield mapping corresponds to an existing Bug field, e.g. severity, the corresponding Bug field will be populated. If not, e.g. build_number, this will be populated in the Bug's custom_fields field. (Leave empty if not using custom fields)", null=True),
        ),
    ]
