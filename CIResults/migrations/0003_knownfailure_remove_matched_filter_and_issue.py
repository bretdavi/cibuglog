# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-03-01 00:26
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('CIResults', '0002_knownfailure_matched_ifa'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='knownfailure',
            name='issue',
        ),
        migrations.RemoveField(
            model_name='knownfailure',
            name='matched_filter',
        ),
        migrations.AlterField(
            model_name='knownfailure',
            name='matched_ifa',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='CIResults.IssueFilterAssociated'),
        ),
    ]
