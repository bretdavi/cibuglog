from arpeggio import Optional, ZeroOrMore, OneOrMore, EOF, ParserPython
from arpeggio import PTNodeVisitor, visit_parse_tree, NoMatch
from arpeggio import RegExMatch as _

from dateutil import parser as datetimeparser
from django.contrib import messages
from django.db.models import Q
from django.utils import timezone
from django.utils.dateparse import parse_duration
from django.utils.functional import cached_property

from shortener.models import Shortener

import traceback
import pytz
import re


# Arpeggio's parser
def val_none(): return _(r'NONE')


def val_int(): return _(r'-?\d+')


def val_str(): return [('"', _(r'[^"]*'), '"'),
                       ("'", _(r"[^']*"), "'")]


def val_bool(): return [r'TRUE', r'FALSE']


def val_datetime(): return 'datetime(', [_(r'[^\)]+')], ')'


def val_duration(): return 'duration(', [_(r'[^\)]+')], ')'


def val_ago(): return 'ago(', [_(r'[^\)]+')], ')'


def val_array(): return "[", OneOrMore(([val_none, val_str, val_int, val_bool, val_datetime, val_duration, val_ago],
                                        Optional(','))), "]"


def nested_expression(): return ZeroOrMore(ZeroOrMore(_(r'[^()]+')), ZeroOrMore("(", nested_expression, ")"),
                                           ZeroOrMore(_(r'[^()]+')))


def val_subquery(): return [('(', nested_expression, ')')]


def filter_field(): return _(r'[a-zA-Z\d_-]+')


def filter_object(): return _(r'\w+'), Optional(".", filter_field)


def basic_filter(): return [(filter_object, ['IS IN', 'NOT IN'], val_array),
                            (filter_object, ['<=', r'<', r'>=', r'>'], [val_int, val_datetime,
                                                                        val_duration, val_ago]),
                            (filter_object, ['=', '!='], [val_duration, val_datetime, val_int,
                                                          val_bool, val_str, val_none]),
                            (filter_object, [r'~=', r'MATCHES', r'CONTAINS', r'ICONTAINS'], val_str),
                            (filter_object, [r'MATCHES'], val_subquery)]


def orderby_object(): return _(r'-?\w+')


def orderby(): return ("ORDER_BY", orderby_object)


def limit(): return ("LIMIT", val_int)


def factor(): return Optional("NOT"), [basic_filter, ("(", expression, ")")]


def expression(): return factor, ZeroOrMore(["AND", "OR"], factor), Optional(orderby), Optional(limit)


def query(): return Optional(expression), EOF


class QueryVisitor(PTNodeVisitor):
    class NoneObject:
        pass

    def __init__(self, model, *arg, **kwargs):
        self.model = model
        self.orderby = None
        self.limit = None

        super().__init__(*arg, **kwargs)

    def visit_val_none(self, node, children):
        # HACK: I would have rather returned None, but Arppegio interprets this as
        # a <no match>... Instead, return a NoneObject that will later be converted
        return QueryVisitor.NoneObject()

    def visit_val_int(self, node, children):
        return FilterObjectInteger.parse_value(node.value)

    def visit_val_str(self, node, children):
        if len(children) == 0:
            return ""
        if len(children) > 1:
            raise ValueError("val_str cannot have more than one child")  # pragma: no cover
        return FilterObjectStr.parse_value(children[0])

    def visit_val_bool(self, node, children):
        return FilterObjectBool.parse_value(node.value)

    def visit_val_datetime(self, node, children):
        if len(children) > 1:
            raise ValueError("val_datetime cannot have more than one child")  # pragma: no cover
        return FilterObjectDateTime.parse_value(children[0])

    def visit_val_duration(self, node, children):
        if len(children) > 1:
            raise ValueError("val_duration cannot have more than one child")  # pragma: no cover
        return FilterObjectDuration.parse_value(children[0])

    def visit_val_ago(self, node, children):
        if len(children) > 1:
            raise ValueError("val_ago cannot have more than one child")  # pragma: no cover
        duration = FilterObjectDuration.parse_value(children[0])
        return timezone.now() - duration

    def visit_filter_field(self, node, children):
        if '__' in node.value:
            raise ValueError("Dict object keys cannot contain the substring '__'")

        return node.value

    def visit_filter_object(self, node, children):
        filter_obj = self.model.filter_objects_to_db.get(children[0])
        if filter_obj is None:
            raise ValueError("The object '{}' does not exist".format(children[0]))

        if isinstance(filter_obj, FilterObjectJSON):
            if len(children) != 2:
                raise ValueError("The dict object '{}' requires a key to access its data".format(children[0]))
            filter_obj = FilterObjectJSON(filter_obj._db_path, filter_obj.description, children[1])
        elif len(children) != 1:
            raise ValueError("The object '{}' cannot have an associated key".format(children[0]))

        return filter_obj

    def visit_val_array(self, node, children):
        return [c for c in children if c != ',']

    def visit_val_subquery(self, node, children):
        out = ""
        for x in list(node):
            out += str(x.flat_str())
        return out

    def visit_basic_filter(self, node, children):
        if len(children) == 3:
            filter_obj, lookup, item = children

            key = filter_obj.db_path

            if isinstance(filter_obj, FilterObjectModel):
                key += '__in'
                item = filter_obj.parse_value(item)
            else:
                if lookup == '<=':
                    key += '__lte'
                elif lookup == '>=':
                    key += '__gte'
                elif lookup == '<':
                    key += '__lt'
                elif lookup == '>':
                    key += '__gt'
                elif lookup == 'CONTAINS':
                    key += '__contains'
                elif lookup == 'ICONTAINS':
                    key += '__icontains'
                elif lookup in ['IS IN', 'NOT IN']:
                    key += '__in'
                elif lookup == 'MATCHES' or lookup == '~=':
                    key += '__regex'
                elif lookup in ['=', '!=']:
                    key += "__exact"
                else:                                                           # pragma: no cover
                    raise ValueError("Unknown lookup '{}'".format(lookup))      # pragma: no cover

            # HACK: see visit_val_none()
            if isinstance(item, QueryVisitor.NoneObject):
                item = None

            obj = Q(**{key: item})
            if lookup in ['!=', 'NOT IN']:
                return ~obj
            else:
                return obj
        else:
            raise ValueError("basic_filter: Invalid amount of operands")    # pragma: no cover

    def visit_factor(self, node, children):
        if len(children) > 1:
            if children[0] == "NOT":
                return ~children[-1]
        return children[-1]

    def visit_orderby_object(self, node, children):
        reverse = node.value[0] == '-'

        obj_name = node.value if not reverse else node.value[1:]

        filter_obj = self.model.filter_objects_to_db.get(obj_name)
        if filter_obj is not None:
            return "{}{}".format("-" if reverse else "", filter_obj.db_path)
        else:
            raise ValueError("The object '{}' does not exist".format(obj_name))

    def visit_orderby(self, node, children):
        if len(children) == 1:
            self.orderby = children[0]
        else:
            raise ValueError("orderby: Invalid amount of operands")    # pragma: no cover

    def visit_limit(self, node, children):
        if len(children) == 1:
            if children[0] < 0:
                raise ValueError("Negative limits are not supported")

            self.limit = children[0]
        else:
            raise ValueError("limit: Invalid amount of operands")    # pragma: no cover

    def visit_expression(self, node, children):
        if len(children) >= 1:
            qResult = children[0]
            for i in range(2, len(children), 2):
                if children[i-1] == "AND":
                    qResult &= children[i]
                elif children[i-1] == "OR":
                    qResult |= children[i]
            return qResult

    def visit_query(self, node, children):
        if len(children) > 1:
            raise ValueError("query cannot have more than one child")  # pragma: no cover
        elif len(children) == 1:
            return children[0]
        else:
            return Q()


class QueryParser:
    def __init__(self, model, user_query):
        self.model = model
        self.user_query = user_query

        self.error = None
        self.q_objects = Q()
        self.orderby = None
        self.limit = None

        try:
            parser = ParserPython(query)
            parse_tree = parser.parse(self.user_query)
            query_visitor = QueryVisitor(self.model)
            self.q_objects = visit_parse_tree(parse_tree, query_visitor)
            self.orderby = query_visitor.orderby
            self.limit = query_visitor.limit
        except ValueError as e:
            self.error = str(e)
        except NoMatch as e:
            self.error = str(e)

    @property
    def query_key(self):
        return Shortener.get_or_create(self.user_query).shorthand

    @property
    def is_valid(self):
        return self.error is None

    @property
    def is_empty(self):
        return not self.is_valid or len(self.user_query) == 0

    @cached_property
    def objects(self):
        if self.is_valid:
            query = self.model.objects.filter(self.q_objects).distinct()
            query = query.order_by(self.orderby) if self.orderby is not None else query
            return query[:self.limit] if self.limit is not None else query
        else:
            return self.model.objects.none()


class LegacyParser:
    userfilters_allowed_lookups = {'exact': '=', 'in': 'IS IN', 'regex': '~=', 'contains': 'CONTAINS',
                                   'icontains': 'ICONTAINS', 'gt': '>', 'gte': '>=', 'lt': '<', 'lte': '<='}
    userfilters_allowed_types = ['str', 'int', 'bool', 'datetime', 'duration']

    def __init__(self, model, **user_filters):
        # Filters should all be of the following format:
        # (only|exclude)__(object)__(in|regex|gt|lt) = str or format(value)
        lookups = "|".join(self.userfilters_allowed_lookups.keys())
        format_re = re.compile((r'(?P<action>(only|exclude))__(?P<object>\w+)__'
                               '(?P<lookup>({lookups}))'.format(lookups=lookups)))

        # Iterate through the user filters, match them to our format regex,
        # then construct the right ORM call
        only = []
        exclude = []
        for key, item in user_filters.items():
            match = format_re.match(key)
            if match:
                fields = match.groupdict()

                db_object = model.filter_objects_to_db.get(fields['object'])
                if db_object is None:
                    continue

                # aggregate all regular expressions into one request
                if fields['lookup'] == 'regex' and isinstance(item, list) and len(item) > 1:
                    item = r'('+'|'.join(item)+')'

                # Try converting the item to the right unit
                item = self._convert_user_values(item)

                bfilter = "{} {} {}".format(fields['object'],
                                            self.userfilters_allowed_lookups.get(fields['lookup']),
                                            item)

                if fields['action'] == 'only':
                    only.append(bfilter)
                else:
                    exclude.append(bfilter)

        self.query = " AND ".join(only)
        if len(exclude) > 0:
            if len(only) > 0:
                self.query += ' AND '
            self.query += "NOT ({})".format(" AND ".join(exclude))

    @classmethod
    def _convert_user_value(cls, value):
        # Will automatically be cached by python
        types = "|".join(cls.userfilters_allowed_types)
        item_re = re.compile(r'(?P<type>({types}))\((?P<value>.*)\)'.format(types=types))

        match = item_re.match(value)
        if match:
            fields = match.groupdict()

            try:
                if fields['type'] == 'str':
                    return "'{}'".format(fields['value'])
                elif fields['type'] == 'bool':
                    return "TRUE" if FilterObjectBool.parse_value(fields['value']) else "FALSE"
                elif fields['type'] == 'int':
                    return fields['value']
                elif fields['type'] == 'datetime' or fields['type'] == 'duration':
                    return value
            except Exception:                 # pragma: no cover
                traceback.print_exc()         # pragma: no cover

        # Default to the variable being a string
        return "'" + value + "'"

    @classmethod
    def _convert_user_values(cls, items):
        # detect whether we have a singular value or a list
        if isinstance(items, list):
            if len(items) > 1:
                new = []
                for item in items:
                    new.append(cls._convert_user_value(item))
                return "[" + ", ".join(new) + "]"
            else:
                return cls._convert_user_value(items[0])
        else:
            return cls._convert_user_value(items)


class UserFiltrableMixin:
    @classmethod
    def _get_value_from_params(cls, user_filters, key):
        val = user_filters.get(key)
        if isinstance(val, list) and len(val) == 1:
            val = val[0]
        return val

    @classmethod
    def from_user_filters(cls, **user_filters):
        query = cls._get_value_from_params(user_filters, 'query')
        if query is None:
            query_key = cls._get_value_from_params(user_filters, 'query_key')
            short = Shortener.objects.filter(shorthand=query_key).first()
            if short is not None:
                query = short.full

        if query is not None:
            return QueryParser(cls, query)
        else:
            query = LegacyParser(cls, **user_filters).query
            return QueryParser(cls, query)


class FilterObject:
    def __init__(self, db_path, description=None):
        self._db_path = db_path
        self._description = description

    @property
    def db_path(self):
        return self._db_path

    @property
    def description(self):
        if self._description is None:
            return "<no description yet>"
        else:
            return self._description


class FilterObjectJSON(FilterObject):
    data_type = "anything"
    documentation = "Expected format: <JSON field>.<key>"
    test_value = "test"

    def __init__(self, db_path, description=None, key=None):
        self.key = key
        super().__init__(db_path, description)

    @property
    def db_path(self):
        if self.key is None:
            raise ValueError("Dict field require a key to be accessed")  # pragma: no cover
        return "{}__{}".format(self._db_path, self.key)


class FilterObjectStr(FilterObject):
    data_type = "string"
    documentation = "Expected format: anything. Use quotes for the new query language (\"\" or '')"
    test_value = "str_test"

    def __init__(self, db_path, description=None):
        super().__init__(db_path, description)

    @classmethod
    def parse_value(self, value):
        return str(value)


class FilterObjectDateTime(FilterObject):
    data_type = "datetime"
    documentation = "Expected format: datetime(YYYY-MM-DD HH:MM[:ss[.uuuuuu]][TZ])"
    test_value = "2019-01-01"

    def __init__(self, db_path, description=None):
        super().__init__(db_path, description)

    @classmethod
    def parse_value(self, value):
        return timezone.make_aware(datetimeparser.parse(value), pytz.utc)


class FilterObjectDuration(FilterObject):
    data_type = "duration"
    documentation = 'Expected format: "duration(DD HH:MM:SS.uuuuuu)", or "duration(P4DT1H15M20S)" (ISO 8601), ' \
                    'or "duration(3 days 04:05:06)" (PostgreSQL).'
    test_value = "123.456 seconds"

    def __init__(self, db_path, description=None):
        super().__init__(db_path, description)

    @classmethod
    def parse_value(cls, value):
        duration = parse_duration(value)
        if duration is None:
            raise ValueError("The value '{}' does not represent a duration. {}".format(value, cls.documentation))
        return duration


class FilterObjectBool(FilterObject):
    data_type = "boolean"
    documentation = "Supported values: bool(false)/bool(0) or bool(true)/bool(1). " \
                    "Use TRUE or FALSE for the new query language."
    test_value = "True"

    def __init__(self, db_path, description=None):
        super().__init__(db_path, description)

    @classmethod
    def parse_value(self, value):
        return str(value).lower() in ["1", "true"]


class FilterObjectInteger(FilterObject):
    data_type = "integer"
    documentation = "Supported values: int(12345). Use 12345 for the new query language."
    test_value = 12345

    def __init__(self, db_path, description=None):
        super().__init__(db_path, description)

    @classmethod
    def parse_value(self, value):
        return int(float(value))


class FilterObjectModel(FilterObject):
    data_type = "subquery"
    documentation = "Expected format: Any query compatible with the model selected"

    def __init__(self, model, db_path, description=None):
        self.model = model
        super().__init__(db_path, description)

    def parse_value(self, value):
        result = QueryParser(self.model, value)
        if not result.is_valid:
            raise ValueError(result.error)
        return result.objects


def request_to_query(request, Model, default_query_parameters={}):
    for params in [request.POST, request.GET]:
        # convert the user filters to a normal dictionary to prevent issues when
        # inserting new values
        requested_filters = params.copy()
        query = Model.from_user_filters(**requested_filters)
        if len(query.user_query) > 0:
            if not query.is_valid:
                messages.error(request, "Filtering error: " + query.error)
            return query

    return Model.from_user_filters(**default_query_parameters)
