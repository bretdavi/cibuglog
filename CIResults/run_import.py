from django.utils.functional import cached_property
from django.core.exceptions import ValidationError
from django.core.validators import URLValidator
from django.db import transaction, connection
from django.db.models import Q
from django.utils import timezone

from CIResults.models import TextStatus, TestResult, TestsuiteRun, RunConfig, RunConfigTag
from CIResults.models import Machine, Test, TestSuite, Component, Build
from CIResults.models import UnknownFailure, KnownFailure, Issue
from CIResults.models import IssueFilterAssociated, RunFilterStatistic

from collections import defaultdict
import configparser
import traceback
import datetime
import copy
import pytz
import time
import bz2
import sys
import os

cur_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(cur_dir, 'piglit'))
from framework import backends  # noqa


def str_to_list(string, separator=' '):
    if string is None:
        return []
    else:
        return [v.strip() for v in string.split(separator) if len(v) > 0]


def validated_url(url):
    if url is None:
        return None

    try:
        URLValidator()(url)
        return url
    except ValidationError:
        return None


class BuildResult:
    def __error__(self, msg):
        raise ValueError(self._error_prefix + msg)

    def __read_file__(self, root_path, rel_path):
        if rel_path is None:
            return None

        if not os.path.isabs(rel_path) and root_path is not None:
            path = os.path.join(root_path, rel_path)
        else:
            path = rel_path

        try:
            if path.endswith(".bz2"):
                with bz2.open(path, 'r') as f:
                    return f.read()
            else:
                with open(path, 'r') as f:
                    return f.read()
        except Exception as e:
            print("Could not open the file '{}': ".format(rel_path, e))

    def __parse_build_ini__(self, build_dir):
        config_path = os.path.join(build_dir, "build.ini")
        self._error_prefix = "The build info file {} is invalid: ".format(config_path)

        config = configparser.ConfigParser()
        config.read(config_path)

        if not config.has_section("CIRESULTS_BUILD"):
            self.__error__("missing the section CIRESULTS_BUILD")

        if len(config.sections()) > 1:
            self.__error__("only the section CIRESULTS_BUILD is allowed")

        c = config["CIRESULTS_BUILD"]
        self._name = c.get('name')
        self._component = c.get('component')
        self._version = c.get('version')
        self._branch = c.get('branch')
        self._repo = c.get('repo')
        self._repo_type = c.get('repo_type')
        self._upstream_url = validated_url(c.get('upstream_url'))
        self._build_log = c.get('build_log')
        self._parents = str_to_list(c.get('parents'))

        # Load the parameters
        self._parameters = c.get("parameters")
        if self._parameters is None:
            self._parameters = self.__read_file__(build_dir,
                                                  c.get("parameters_file"))

        # Load the logs
        self._build_log = self.__read_file__(build_dir,
                                             c.get("build_log_file"))

    def __init__(self, build_dir=None, name=None, component=None, version=None,
                 branch=None, repo=None, repo_type=None, upstream_url=None,
                 parameters=None, config_file=None, build_log_file=None,
                 parents=[]):

        # Either parse the build.ini file, or reconstruct it!
        if build_dir is not None:
            self.__parse_build_ini__(build_dir)
        else:
            self._error_prefix = ""
            self._name = name
            self._component = component
            self._version = version
            self._branch = branch
            self._repo = repo
            self._repo_type = repo_type
            self._upstream_url = validated_url(upstream_url)
            self._parents = parents if parents is not None else []

            self._parameters = parameters
            if self._parameters is None:
                self._parameters = self.__read_file__(None, config_file)
            self._build_log = self.__read_file__(None, build_log_file)

        # Parse the main section
        for field in ['name', 'component', 'version']:
            val = getattr(self, "_" + field)
            if val is None:
                self.__error__("the required field '{}' is missing".format(field))

    def load_resource(self, obj_manager, name):
        try:
            return obj_manager.objects.get(name=name)
        except Exception:
            msg = "Could not find the object named '{}' in {}. Please create it first!"
            raise ValueError(msg.format(name, obj_manager))

    @transaction.atomic
    def commit_to_db(self):
        # Check if the build already exists before creating a new one
        try:
            build = Build.objects.get(name=self._name)
            print("The build named '{}' already exists. Nothing to do...".format(self._name))
            return
        except Build.DoesNotExist:
            pass

        # Get all the dependencies for this build
        component = self.load_resource(Component, self._component)
        parents = []
        for parent in self._parents:
            try:
                parents.append(self.load_resource(Build, parent))
            except ValueError:
                print("The parent build '{}' cannot be found. Ignoring...".format(parent))

        # Create the build
        build = Build(name=self._name, component=component, version=self._version,
                      branch=self._branch, repo_type=self._repo_type, repo=self._repo,
                      upstream_url=self._upstream_url, parameters=self._parameters,
                      build_log=self._build_log)
        build.save()

        # Finish by adding all the parents (had to be done after the object was created)
        build.parents.add(*parents)


class TestsuiteTestResult:
    def __init__(self, name, status, start_time, duration, command=None,
                 stdout=None, stderr=None, dmesg=None, url=None):
        self._name = name
        self._status = status
        self._start_time = start_time
        self._duration = duration
        self._command = command
        self._stdout = stdout
        self._stderr = stderr
        self._dmesg = dmesg
        self._url = url

    @property
    def name(self):
        return self._name

    @property
    def status(self):
        return self._status

    @property
    def start_time(self):
        return self._start_time

    @property
    def duration(self):
        return self._duration

    @property
    def command(self):
        return self._command

    @property
    def stdout(self):
        return self._stdout

    @property
    def stderr(self):
        return self._stderr

    @property
    def dmesg(self):
        return self._dmesg

    @property
    def url(self):
        return self._url


class TestsuiteRunResults:
    def __init__(self, testsuite, machine_name, run_id, test_results, start_time, duration):
        self._testsuite = testsuite
        self._machine_name = machine_name
        self._run_id = run_id
        self._test_results = test_results
        self._start_time = start_time
        self._duration = duration

    @classmethod
    def __result_url__(self, testsuite, run_id, machine_name, test_name):
        # Generate the testresult's external URL
        url_pattern = testsuite.result_url_pattern
        url = url_pattern.format(runconfig=testsuite.runconfig.name,
                                 testsuite_build=testsuite.build,
                                 run_id=run_id, test=test_name,
                                 machine=machine_name)
        return validated_url(url)

    @property
    def testsuite(self):
        return self._testsuite

    @property
    def machine_name(self):
        return self._machine_name

    @property
    def run_id(self):
        return self._run_id

    @property
    def test_results(self):
        return self._test_results

    @property
    def start_time(self):
        return self._start_time

    @property
    def duration(self):
        return self._duration

    @cached_property
    def tests_set(self):
        s = set()
        for test_result in self._test_results:
            s.add(test_result.name)
        return s

    @cached_property
    def statuses_set(self):
        s = set()
        for test_result in self._test_results:
            s.add(test_result.status)
        return s


class PiglitResult(TestsuiteRunResults):
    def __init__(self, testsuite, machine_name, run_id, dir_name):
        testresults = []

        try:
            results = backends.load(dir_name)
            test_duration_sum = datetime.timedelta()
            for test_name in results.tests:
                test = results.tests[test_name]

                url = self.__result_url__(testsuite, run_id, machine_name, test_name)
                start_time = datetime.datetime.fromtimestamp(test.time.start, tz=pytz.utc)
                duration = datetime.timedelta(seconds=test.time.total)
                test_duration_sum += duration
                testresults.append(TestsuiteTestResult(name=test_name, status=test.result,
                                                       start_time=start_time,
                                                       duration=duration,
                                                       command=test.command,
                                                       stdout=str(test.out),
                                                       stderr=str(test.err),
                                                       dmesg=test.dmesg,
                                                       url=url))

            start = datetime.datetime.fromtimestamp(results.time_elapsed.start, tz=pytz.utc)
            duration = datetime.timedelta(seconds=results.time_elapsed.total)

            # Make sure the total duration is at least as long as the sum of all the test executions
            if duration < test_duration_sum:
                duration = test_duration_sum
        except IndexError:
            start = datetime.datetime.fromtimestamp(0, tz=pytz.utc)
            duration = datetime.timedelta(seconds=0)

        super().__init__(testsuite, machine_name, run_id, testresults,
                         start, duration)


class TestsuiteResults:
    def __init__(self, runconfig, name, build, format, version, result_url_pattern):
        self._runconfig = runconfig
        self._name = name
        self._build = build
        self._format = format
        self._version = version
        self._result_url_pattern = result_url_pattern

        # Check if the database contains the build, and then fetch the Testsuite
        # associated with it
        build = Build.objects.get(name=build)
        self._db_object = TestSuite.objects.get(name=build.component)

        # Now check the result format
        if format == "piglit":
            if version != 1:
                msg = "The version {} of the testsuite result format '{}' is unsupported"
                raise ValueError(msg.format(version, format))
            self._result_type = PiglitResult
        else:
            raise ValueError("The testsuite result format '{}' is unsupported".format(format))

    @property
    def runconfig(self):
        return self._runconfig

    @property
    def name(self):
        return self._name

    @property
    def build(self):
        return self._build

    @property
    def format(self):
        return self._format

    @property
    def version(self):
        return self._version

    @property
    def result_url_pattern(self):
        return self._result_url_pattern

    @property
    def db_object(self):
        return self._db_object

    def read_results(self, machine, run_id, path):
        return self._result_type(self, machine, run_id, path)


class TestSuiteRunDef:
    def __to_int__(self, field_name, value):
        try:
            return int(value)
        except Exception:
            raise ValueError("The parameter {} '{}' should be an integer".format(field_name, value))

    def __init__(self, testsuite_build, results_format, results_format_version,
                 machine, ts_run_id, ts_run_path):
        self.testsuite_build = testsuite_build
        self.results_format = results_format
        self.results_format_version = self.__to_int__("results_format_version", results_format_version)
        self.machine = machine
        self.ts_run_id = self.__to_int__("ts_run_id", ts_run_id)
        self.ts_run_path = ts_run_path

        for field in ["testsuite_build", "results_format", "machine", "ts_run_id", "ts_run_path"]:
            if getattr(self, field) is None:
                raise ValueError("The parameter {} cannot be None".format(field))


# WARNING: This function is tailored for the purpose of adding results!
#          DO NOT USE AS A LIGHTWEIGHT REPLACEMENT FOR Issue.update_statistics()
def issue_simple_stats_recomputing(issue, runconfig, stats_changes):
    # Iterate through all the filters associated to this issue and check if
    # a filter's statistic has changed from not covered/affected to
    # covered/affected. If so, and if the issue was not already
    # covered/affected, update the statistics by directly doing +1 in the
    # relevant stats field. Since we are only adding results, we cannot be in a
    # situation where we need to -1 an issue.
    was_covered = False
    has_new_filter_covering = False
    was_affected = False
    has_new_filter_matched = False
    for issuefilter in issue.filters.all():
        prev, new = stats_changes.get(issuefilter, (None, None))

        # If we have no previous stats for the filter, just fake empty ones
        if prev is None:
            prev = RunFilterStatistic(filter=issuefilter, runconfig=runconfig,
                                      matched_count=0, covered_count=0)

        # Check if the issue was covered/affected for this runconfig before
        if prev.covered_count > 0:
            was_covered = True
            if prev.matched_count > 0:
                was_affected = True
                # OPTIMIZATION: The issue was already affected, so no changes in
                # statistics could come by adding more results
                return

        if new is not None:
            if prev.covered_count == 0 and new.covered_count > 0:
                has_new_filter_covering = True
            if prev.matched_count == 0 and new.matched_count > 0:
                has_new_filter_matched = True

    # Update the covered/affected count if necessary
    changed = False
    if not was_covered and has_new_filter_covering:
        issue.runconfigs_covered_count += 1
        changed = True

    if not was_affected and has_new_filter_matched:
        issue.runconfigs_affected_count += 1
        issue.last_seen = runconfig.added_on
        issue.last_seen_runconfig = runconfig
        changed = True

    if changed:
        issue.save()


class RunConfigResults:
    def __error__(self, msg):
        raise ValueError(self._error_prefix + msg)

    def __init__(self, runconfig_dir=None, name=None, url=None,
                 result_url_pattern=None, environment=None, builds=[], tags=[],
                 results=[], temporary=False):
        self._run_results = []

        if runconfig_dir is not None:
            self.__parse_run_info__(runconfig_dir)
            if self._testsuites.get("CIRESULTS_TESTSUITE") is not None:
                # DEPRECATED: Do not use this mode as it will be removed
                self.__load_results_single_testsuite__(runconfig_dir)
            else:
                self.__load_results_multiple_testsuites__(runconfig_dir)
        else:
            self._error_prefix = ""
            self._name = name
            self._url = url
            self._result_url_pattern = result_url_pattern
            self._environment = environment
            self._builds = builds
            self._tags = tags
            self._temporary = temporary

            if self._name is None:
                self.__error__("runconfig name unspecified")

            self.__import_results_from_args__(results)

    def __import_results_from_args__(self, results):
        self._testsuites = dict()
        build_to_testsuite = dict()
        ts_runs = set()

        for r in results:
            # Check that the testsuite build is in the list of builds of the runconfig
            if r.testsuite_build not in self.builds:
                raise ValueError("The build named '{}' is not part of the list of "
                                 "builds of the runconfig".format(r.testsuite_build))

            # Get the testsuite associated to the build name, or create it
            tsp = build_to_testsuite.get((r.testsuite_build, r.results_format,
                                          r.results_format_version))
            if tsp is None:
                try:
                    testsuite_name = Build.objects.get(name=r.testsuite_build).component.name

                    # Create the testsuite results object
                    tsp = TestsuiteResults(self, testsuite_name, r.testsuite_build, r.results_format,
                                           r.results_format_version, self._result_url_pattern)
                    self._testsuites[(r.testsuite_build, r.results_format, r.results_format_version)] = tsp

                    build_to_testsuite[r.testsuite_build] = tsp
                except Build.DoesNotExist:
                    raise ValueError("The build named '{}' does not exist".format(r.testsuite_build))

            # Check that the testsuite run has not been added already
            ts_run_key = (testsuite_name, r.ts_run_id, self._name, r.machine)
            if ts_run_key in ts_runs:
                msg = "Try to import twice {}'s run ID {} on the runconfig '{}' for the machine '{}'"
                raise ValueError(msg.format(testsuite_name, r.ts_run_id, self._name, r.machine))
            else:
                ts_runs.add(ts_run_key)

            # Import the results
            try:
                self._run_results.append(tsp.read_results(r.machine, r.ts_run_id, r.ts_run_path))
            except FileNotFoundError:
                pass
            except Exception:
                traceback.print_exc()

    def __parse_run_info__(self, runconfig_dir):
        conf_path = os.path.join(runconfig_dir, "runconfig.ini")
        self._error_prefix = "The RunConfig file {} is invalid: ".format(conf_path)

        config = configparser.ConfigParser()
        config.read(conf_path)

        if not config.has_section("CIRESULTS_RUNCONFIG"):
            self.__error__("missing the section CIRESULTS_RUNCONFIG")

        # Parse the main section
        section = config["CIRESULTS_RUNCONFIG"]
        self._name = section.get('name')
        self._url = validated_url(section.get('url'))
        self._result_url_pattern = section.get('result_url_pattern', '')
        self._environment = section.get('environment')
        self._builds = str_to_list(section.get("builds"))
        self._tags = str_to_list(section.get("tags"))
        self._temporary = section.getboolean('temporary', False)

        # Check that if CIRESULTS_TESTSUITE is set, then no other sections are there
        if "CIRESULTS_TESTSUITE" in config.sections() and len(config.sections()) > 2:
            msg = "If the section CIRESULTS_TESTSUITE exists, then no additional section/testsuite can be added"
            self.__error__(msg)

        # Parse the testsuite sections
        self._testsuites = dict()
        for section in config.sections():
            # Ignore the main section
            if section == "CIRESULTS_RUNCONFIG":
                continue

            build = config[section]['build']
            if build not in self._builds:
                msg = "The build '{}' of the testsuite '{}' is not found in the list of builds of the runconfig {}"
                self.__error__(msg.format(build, section, self._name))
            format = config[section]['format']
            version = config[section].getint('version', 1)
            result_url_pattern = config[section].get('result_url_pattern', '')
            self._testsuites[section] = TestsuiteResults(self, section, build,
                                                         format, version,
                                                         result_url_pattern)

        if self._name is None:
            self.__error__("runconfig name unspecified")

    def __load_testsuite_results__(self, tsp, testsuite_path):
        # Since we have a valid test suite, we can now continue and look for
        # all the machines that run any test
        for machine in [f for f in os.listdir(testsuite_path)]:
            machine_path = os.path.join(testsuite_path, machine)
            if os.path.isfile(machine_path):
                continue

            for ts_run_id in os.listdir(machine_path):
                ts_run_path = os.path.join(machine_path, ts_run_id)
                if os.path.isfile(ts_run_path):
                    continue

                # Convert the run_id to integer, or ignore the result
                try:
                    ts_run_id = int(ts_run_id)
                except Exception:
                    print("RunConfigResults: testsuite run ID '{}' should be an integer".format(ts_run_id))
                    continue

                try:
                    self._run_results.append(tsp.read_results(machine, ts_run_id, ts_run_path))
                except FileNotFoundError:
                    pass
                except Exception:
                    traceback.print_exc()

    def __load_results_single_testsuite__(self, runconfig_dir):
        tsp = self._testsuites.get("CIRESULTS_TESTSUITE")
        self.__load_testsuite_results__(tsp, runconfig_dir)

    def __load_results_multiple_testsuites__(self, runconfig_dir):
        for testsuite_name in [f for f in os.listdir(runconfig_dir)]:
            testsuite_path = os.path.join(runconfig_dir, testsuite_name)
            if os.path.isfile(testsuite_path):
                continue

            tsp = self._testsuites.get(testsuite_name)
            if tsp is None:
                print("Ignore the testsuite '{}' because it is not listed in the runconfig file".format(testsuite_name))
                continue

            self.__load_testsuite_results__(tsp, testsuite_path)

    @property
    def name(self):
        return self._name

    @property
    def url(self):
        return self._url

    @property
    def environment(self):
        return self._environment

    @property
    def builds(self):
        return self._builds

    @property
    def tags(self):
        return self._tags

    @property
    def temporary(self):
        return self._temporary

    @property
    def testsuites(self):
        return self._testsuites

    @cached_property
    def tests(self):
        tests = dict()
        for run in self._run_results:
            db_testsuite = run.testsuite.db_object
            if db_testsuite not in tests:
                tests[db_testsuite] = set()
            tests[db_testsuite] |= run.tests_set
        return tests

    @cached_property
    def machines(self):
        machines = set()
        for run in self._run_results:
            machines.add(run.machine_name)
        return machines

    @cached_property
    def text_statuses(self):
        statuses = dict()
        for run in self._run_results:
            db_testsuite = run.testsuite.db_object
            if db_testsuite not in statuses:
                statuses[db_testsuite] = set()
            statuses[db_testsuite] |= run.statuses_set
        return statuses

    def __preload_resources__(self, obj_manager, names):
        ret = dict()
        try:
            if type(names) == list:
                for name in names:
                    ret[name] = obj_manager.get(name=name)
            elif type(names) == str:
                ret[names] = obj_manager.get(name=name)
        except Exception as e:
            raise ValueError("The object {} does not exist in the database".format(name)) from e
        return ret

    def __add_missing__(self, obj_type, obj_str, objs, key_field, args, filter={}):
        # Fetch the current list of objects
        db_objs = set([getattr(o, key_field) for o in obj_type.objects.filter(**filter)])

        # Add the missing objects
        to_add = []
        for obj in (objs - db_objs):
            args[key_field] = obj
            to_add.append(obj_type(**args))
        if len(to_add) > 0:
            print("adding {} missing {}".format(len(to_add), obj_str))
            obj_type.objects.bulk_create(to_add)

        # Now fetch all the objetcs and index them, making sure we do not add
        # unecessary objects
        ret = dict()
        for entry in obj_type.objects.filter(**filter):
            key = getattr(entry, key_field)
            if key in objs:
                ret[key] = entry
        return ret

    def __ts_runs_to_dict__(self, runconfig):
        ret = dict()
        for r in TestsuiteRun.objects.filter(runconfig=runconfig):
            ret[(r.testsuite.name, r.machine.name, r.run_id)] = r
        return ret

    @transaction.atomic
    def commit_to_db(self, new_machines_public=False, new_tests_public=False,
                     new_machines_vetted=False, new_tests_vetted=False,
                     new_statuses_vetted=False, quiet=False):
        now = timezone.now()

        # Pre-fetch all the resources
        db_builds = self.__preload_resources__(Build.objects, self._builds)
        db_tags = self.__preload_resources__(RunConfigTag.objects, self._tags)

        # Create the runconfig if it does not exists
        try:
            runconfig = RunConfig.objects.get(name=self._name)
            cur_builds = set(runconfig.builds.all())
        except Exception:
            # Create the run with the proper URL
            runconfig = RunConfig(name=self._name, url=self.url,
                                  environment=self.environment,
                                  temporary=self._temporary)
            runconfig.save()

            # Add it to all the tags
            for tag in db_tags.values():
                runconfig.tags.add(tag)

            # We have no current builds
            cur_builds = set()

        # Verify that we do not have two builds for the same component
        components = dict()
        for build in set(db_builds.values()) | cur_builds:
            if build.component not in components:
                components[build.component] = build
            else:
                msg = "ERROR: Two builds ({} and {}) cannot be from the same component ({})"
                self.__error__(msg.format(build, components[build.component], build.component))

        # Add all the new builds
        for new_build in set(db_builds.values()) - cur_builds:
            runconfig.builds.add(new_build)

        # Abort early if there is nothing else to do
        if len(self._run_results) == 0:
            print("No results to add, exiting...")
            return

        # Load all the existing testsuite runs on this runconfig
        db_ts_runs = dict()
        tsr_filter = TestsuiteRun.objects.filter(runconfig__name=self._name)
        for ts_run in tsr_filter.prefetch_related("machine"):
            if ts_run.machine.name not in db_ts_runs:
                db_ts_runs[ts_run.machine.name] = set()
            db_ts_runs[ts_run.machine.name].add((ts_run.testsuite.name, ts_run.run_id))

        # Add the missing machines, tests and statuses
        db_machines = self.__add_missing__(Machine, "machine(s)", self.machines,
                                           'name', {"public": new_machines_public,
                                                    "vetted_on": now if new_machines_vetted else None})
        db_tests = dict()
        for testsuite in self.tests:
            db_tests[testsuite.name] = self.__add_missing__(Test,
                                                            "test(s) ({})".format(testsuite),
                                                            self.tests[testsuite], 'name',
                                                            {"public": new_tests_public,
                                                             "testsuite": testsuite,
                                                             "vetted_on": now if new_tests_vetted else None},
                                                            filter={"testsuite": testsuite})

        # If the runconfig is non-temporary, convert all the tests
        if not self._temporary:
            cur_tests_ids = list()
            for ts in db_tests.values():
                cur_tests_ids.extend([t.id for t in ts.values()])
            Test.objects.filter(pk__in=cur_tests_ids, first_runconfig=None).update(first_runconfig=runconfig)

        db_statuses = dict()
        for testsuite in self.text_statuses:
            statuses = self.__add_missing__(TextStatus,
                                            "statuse(s) ({})".format(testsuite),
                                            self.text_statuses[testsuite],
                                            'name', {"testsuite": testsuite,
                                                     "vetted_on": now if new_statuses_vetted else None},
                                            filter={"testsuite": testsuite})
            db_statuses[testsuite.name] = statuses

        # Create all the ts_runs
        new_ts_runs = []
        to_add = []
        for run in self._run_results:
            # Ignore the runs we already have
            if run.machine_name in db_ts_runs and (run.testsuite.name, run.run_id) in db_ts_runs[run.machine_name]:
                continue

            # Create the ts_run object
            s = TestsuiteRun(testsuite=run.testsuite.db_object,
                             runconfig=runconfig, machine=db_machines[run.machine_name],
                             run_id=run.run_id, start=run.start_time, duration=run.duration)
            to_add.append(s)
            new_ts_runs.append(run)
        if len(to_add) > 0:
            print("adding {} testsuite runs".format(len(to_add)))
            TestsuiteRun.objects.bulk_create(to_add)
        ts_runs_db = self.__ts_runs_to_dict__(runconfig)

        # Create all the TestResults and find the failures
        new_results = []
        failures = []
        for run in new_ts_runs:
            db_ts_run = ts_runs_db[(run.testsuite.name, run.machine_name, run.run_id)]
            for result in run.test_results:
                ts = run.testsuite.db_object
                t = TestResult(test=db_tests[ts.name][result.name],
                               ts_run=db_ts_run,
                               status=db_statuses[ts.name][result.status],
                               start=result.start_time, duration=result.duration,
                               command=result.command,
                               stdout=result.stdout,
                               stderr=result.stderr,
                               dmesg=result.dmesg,
                               url=result.url)
                new_results.append(t)

                if ts.is_failure(t.status):
                    failures.append(t)
        if len(new_results) > 0:
            print("adding {} test results".format(len(new_results)))

            # We'll need the primary IDs later, so bulk_create on postgresql
            # and not on the others (since they do not support retrieving it).
            if connection.vendor == "postgresql":
                TestResult.objects.bulk_create(new_results, batch_size=5000)
            else:
                for result in new_results:
                    result.save()

        # Fetch all the associated IssueFilters and their related issues
        db_filters = dict()
        active_ifas = IssueFilterAssociated.objects_ready_for_matching.filter(Q(deleted_on=None))
        for e in active_ifas:
            if e.filter not in db_filters:
                db_filters[e.filter] = list()
            db_filters[e.filter].append(e)

        # Map the failures
        start = time.time()
        known_failures = []
        unknown_failures = []

        # Get the filters statistics already-existing on this runconfig and lock them for update
        stats_changes = dict()  # stores results as a tuple (current, new)
        if not self.temporary:
            for stat in RunFilterStatistic.objects.select_for_update().filter(runconfig=runconfig):
                stats_changes[stat.filter] = (stat, None)

        for result in failures:
            found = False
            for issuefilter in db_filters.keys():
                # start matching the result to the current filter
                if issuefilter.covers(result):
                    # Get or create a statistics object for the current filter and runconfig
                    fs = stats_changes.get(issuefilter, (None, None))
                    if fs[1] is None:
                        if fs[0] is not None:
                            fs = (fs[0], copy.copy(fs[0]))
                        else:
                            stats_changes[issuefilter] = fs = (None, RunFilterStatistic(filter=issuefilter,
                                                                                        runconfig=runconfig,
                                                                                        matched_count=0,
                                                                                        covered_count=0))

                    fs[1].covered_count += 1

                    if issuefilter.matches(result, skip_cover_test=True):
                        fs[1].matched_count += 1
                        for ifa in db_filters[issuefilter]:
                            known_failures.append(KnownFailure(result=result,
                                                               matched_ifa=ifa))
                        found = True
            if not found:
                unknown_failures.append(UnknownFailure(result=result))

        msg = "Found {} test failures ({} filters matched, {} failures left unmatched) in {:.2f} ms"
        print(msg.format(len(failures), len(known_failures), len(unknown_failures), (time.time() - start) * 1000))
        KnownFailure.objects.bulk_create(known_failures)
        UnknownFailure.objects.bulk_create(unknown_failures)

        # Create the statistics objects, but only if the run was not temporary
        if not self.temporary:
            # Create a new transaction, in case the creation of stats fail
            try:
                with transaction.atomic():
                    # Save all the stats (WARNING: some might already exist in the DB, hence the call to .save())
                    new_stats = [fs[1] for fs in stats_changes.values() if fs[1] is not None]
                    print("Updating the statistics of {} filters".format(len(new_stats)))
                    start = time.time()
                    for stat in new_stats:
                        try:
                            stat.save()
                        except Exception:
                            traceback.print_exc()
                    print("Filter statistics updated in {:.2f} ms".format((time.time() - start) * 1000))

                    # Get the list of issues that need to be updated because the filters'
                    # statistics got updated
                    issues = Issue.objects.select_for_update().filter(filters__in=stats_changes.keys())
                    print("Updating the statistics of {} issues".format(len(issues)))
                    start = time.time()
                    for issue in issues.prefetch_related('filters'):
                        issue_simple_stats_recomputing(issue, runconfig, stats_changes)
                    print("Issue statistics updated in {:.2f} ms".format((time.time() - start) * 1000))
            except Exception:
                traceback.print_exc()

        # Go through all the unknown failures
        if len(unknown_failures) > 0:
            # Fetch all archived IFAs that are less than 6 months old
            db_archived_filters = defaultdict(list)
            archived_threshold = now - datetime.timedelta(days=180)
            a_ifas = IssueFilterAssociated.objects.exclude(deleted_on=None).exclude(deleted_on__gt=archived_threshold)
            for e in a_ifas:
                db_archived_filters[e.filter].append(e)

            # Try to match the unknown failures with archived IFAs
            start = time.time()
            filters_matching = set()
            for failure in unknown_failures:
                for issuefilter in db_archived_filters.keys():
                    if issuefilter.matches(failure.result):
                        failure.matched_archived_ifas.add(*db_archived_filters[issuefilter])
                        filters_matching.add(issuefilter)

            msg = "Found {}/{} recently-archived filters matching some unknown failures in {:.2f} ms"
            print(msg.format(len(filters_matching), len(a_ifas), (time.time() - start) * 1000))
