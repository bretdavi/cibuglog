var dummy_script = `def replication_check(src_bug, dest_bug):
    """This a very trivial example script"""
    if int(src_bug['bug_id']) % 2 == 0:
        return {"set_fields": {'description': src_bug['description'],'title': src_bug['title']},
                "add_comments": ["hello","world","foo"]}
    else:
        return {}`;

function build_bug_row(bug, bug_data){
  var bug_link = "<td><a href=\"" + bug.url + "\">" + bug.bug_id + "</a></td>"
  var bug_status = "<td>" + bug.status + "</td>"
  var bug_title = "<td>" + bug.title + "</td>"
  var bug_priority = "<td>" + bug.priority + "</td>"
  var bug_operation = "<td>" + bug_data["operation"] + "</td>"
  var bug_fields = '<td id=json_field>' + JSON.stringify(bug_data["set_fields"]) + '</td>'
  var bug_comments = "<td id=json_comms>" + JSON.stringify(bug_data["add_comments"]) + "</td>"
  return bug_link + bug_status + bug_title + bug_priority + bug_operation + bug_fields + bug_comments
}

function format_upd_bug(bug){
  var upd_table = $("<table class=\"table\"></table>")
  var tab_head = "<thead><tr><th scope=\"col\">Replicated Bug ID</th>"+
                 "<th scope=\"col\">Status</th><th scope=\"col\">Title</th>"+
                 "<th scope=\"col\">Priority</th></tr></thead>"
  var bug_link = "<td><a href=\"" + bug.url + "\">" + bug.bug_id + "</a></td>"
  var bug_status = "<td>" + bug.status + "</td>"
  var bug_title = "<td>" + bug.title + "</td>"
  var bug_priority = "<td>" + bug.priority + "</td>"
  var full_str = "<tbody><tr>" + bug_link + bug_status + bug_title + bug_priority + "</tr></tbody>"
  upd_table.append(tab_head)
  upd_table.append(full_str)
  return upd_table
}

function python_editor(script_box) {
  var editor = CodeMirror.fromTextArea(script_box, {
    mode: {name: "python",
    version: 3,
    singleLineStringErrors: false},
    lineNumbers: true,
    indentUnit: 4,
    matchBrackets: true,
    extraKeys: {
      "F11": function(cm) {
        cm.setOption("fullScreen", !cm.getOption("fullScreen"));
      },
      "Esc": function(cm) {
        if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
      }
    }
  });

  return editor
}

function init_diff(view_id, editor) {
  var target = document.getElementById(view_id);
  target.innerHTML = "";
  dv = CodeMirror.MergeView(target, {
    value: "",
    orig: editor.getValue(),
    lineNumbers: true,
    mode: "python",
    indentUnit: 4,
    highlightDifferences: true,
    viewportMargin: 20
  })

  return dv
}

function check_script(scr, src_tracker, dest_tracker, dtable, url, loader, token){
  var params = {
      url: url,
      type: 'POST',
      headers: {
        'X-CSRFToken': token
      },
      data: {
        'script': scr,
        'source_tracker': src_tracker,
        'destination_tracker': dest_tracker
      },
      dataType: 'json',
      beforeSend: function() {
        dtable.clear().draw()
        loader.start();
      },
      success: function(data) {
        $(".save_btn").show()
        loader.stop();
        $.each(data['bugs'], function(i, bug_data) {
          var row = $("<tr></tr>")
          var row_str = build_bug_row(bug_data["src_bug"], bug_data)
          row.html(row_str)
          renderjson.set_max_string_length(50)
          row.children("#json_field").data(bug_data["set_fields"])
          row.children("#json_comms").data(bug_data["add_comments"])

          if (bug_data["operation"] === "update") {
            upd_bug = bug_data["dest_bug"]
            row.data("update_bug", upd_bug)
            var upd_col = row.find("td:eq(4)")
            upd_col.addClass("updated-bug")
            upd_col.text("")
            upd_col.append('update <span class="glyphicon glyphicon-plus-sign" style="color:blue;"></span>')
          }
          dtable.row.add(row)
        })
        dtable.draw()
      },
      error: function(xhr, ajaxOptions, respError) {
        $(".save_btn").hide()
        loader.stop();
        alert("Script execution failure: " + jQuery.parseJSON(xhr.responseText)["message"])
      }
  };

  $.ajax(params)
}

function setup_import_script(import_btn, editor) {
  $(import_btn).on('change', function(){
      var fileReader = new FileReader();
      fileReader.onload = function () {
        var data = fileReader.result
        editor.setValue(data)
      }
      fileReader.readAsText($(import_btn).prop('files')[0])
  })
}

function setup_export_script(export_btn, editor, filename) {
  $(export_btn).click(function(){
    var scr = editor.getValue()
    var fname = filename
    var blob = new Blob([scr], {type:"text/plain;charset=utf-8"})
    saveAs(blob, fname)
  })
}

function child_listener(table_id, table) {
  $(table_id).on('click', 'td.updated-bug', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
        var upd_bug = $(tr).data("update_bug");

        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            row.child( format_upd_bug(upd_bug) ).show();
            tr.addClass('shown');
        }
    } );
}

function create_datatable(table_id) {
  return $(table_id).DataTable( {
    columnDefs: [ {
      targets: 5,
      createdCell: function(td, cellData, rowData, row, col) {
        $(td).html($(renderjson($(td).data())))
      }
    },
    {
      targets: 6,
      createdCell: function(td, cellData, rowData, row, col) {
        $(td).html($(renderjson($(td).data())))
      }
    }],
    buttons: [
      { "extend": 'excel',
        "exportOptions": { "orthogonal" : 'export'},
        "text": ' Excel',
        "className": 'glyphicon glyphicon-download-alt'
      },
      { "extend": 'csv',
        "exportOptions": { "orthogonal" : 'export'},
        "text": ' CSV',
        "className": 'glyphicon glyphicon-download-alt'
      }
    ]
  });
}
