from django.test import TestCase
from unittest.mock import patch, call

from CIResults.models import TestSuite, Test

from CIResults.forms import TestMassRenameForm


class Test_TestMassRenameForm(TestCase):
    @patch('CIResults.models.Test.rename')
    def test_empty_form(self, mock_rename):
        form = TestMassRenameForm({})

        self.assertFalse(form.is_valid())
        self.assertEqual(form.affected_tests, dict())

        form.do_renaming()
        mock_rename.assert_not_called()

    @patch('CIResults.models.Test.rename')
    def test_valid_form(self, mock_rename):
        tests = []
        ts = TestSuite.objects.create(name="testsuite", public=True)
        for i in range(4):
            name = "{}test_{}".format("old" if i % 2 == 0 else "", i)
            tests.append(Test.objects.create(testsuite=ts, name=name, public=True))

        form = TestMassRenameForm({'substring_from': 'oldtest_', 'substring_to': 'newtest_'})
        self.assertTrue(form.is_valid())
        self.assertEqual(form.affected_tests, {tests[0]: 'newtest_0', tests[2]: 'newtest_2'})

        form.do_renaming()
        self.assertEqual(mock_rename.call_args_list, [call('newtest_0'), call('newtest_2')])
