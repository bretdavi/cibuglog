.. CI Bug Log documentation master file, created by
   sphinx-quickstart on Thu Mar 12 15:31:52 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. mdinclude:: ../README.md

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   CIResults/CIResults.rst
   replication/replication.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
