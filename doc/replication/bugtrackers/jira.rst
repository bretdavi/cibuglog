.. _jira-api-doc:

Jira Replication
================
Handling of bugs in Jira is done using the jira-python library. When creating/updating bugs, the format
is essentially the same as what would be used in that library, though there are certain derivations from
that API, which will be covered in this page.

The documentation for the jira-python api can be found `here <https://jira.readthedocs.io/en/master/>`_

Creating Bugs
-------------

The bugs are created via the `create_issue <https://jira.readthedocs.io/en/master/api.html#jira.JIRA.create_issue>`_ method.
A dictionary is passed to the method which defines all the fields to set when creating the Bug. This dictionary is pulled
from the dictionary returned by the replication check method under the 'set_fields' key, e.g.

.. code-block:: python

  {'set_fields': {"<fields dictionary passed to create_issue>"}}

.. _create-pre-pop:

Pre-populated Fields
^^^^^^^^^^^^^^^^^^^^

The dictionary under 'set_fields' does get updated to include certain fields, so these do not need to be set in the replication
script.

The 'project' field gets set with the project key set in the BugTracker object in CI Bug Log, so this does not need to be specified.

The 'issuetype' field can be set in the replication script, but if not, it defaults to 'Bug'

.. _spec-fields:

Special Fields
^^^^^^^^^^^^^^

To allow for additional operations that can't be performed directly from the create_issue method, there are special fields that can
be used to handle these.

To allow for transitioning the state of a bug, the `transition_issue <https://jira.readthedocs.io/en/master/api.html#jira.JIRA.transition_issue>`_
method needs to be used. To specify a transition operation, add a field 'transition' to the fields dictionary that stores a dictionary as shown
below:

.. code-block:: python

  {'transition': {'status': "<transition name or ID>", 'fields': "<fields on the transition menu to set>"}}

This dictionary will get used to call the transition method, if it is specified. If you do not need to transition the bug, the 'transition' field
can simply be omitted.

Updating Bugs
-------------

The bugs are updated via the Issue `update <https://jira.readthedocs.io/en/master/api.html#jira.Issue.update>`_ method. The same format
is used as described in the `Creating Bugs`_ section.

Pre-populated Fields
^^^^^^^^^^^^^^^^^^^^

The 'project' field gets set just as described in :ref:`create-pre-pop`

Special Fields
^^^^^^^^^^^^^^

The 'transition' field is supported as described in :ref:`spec-fields`

In addition to this, there is also an 'update' field that has a special meaning in the update method. Below is the dictionary format
to clarify

.. code-block:: python

  {'set_fields': {'fields': "<fields to set>", 'update': "<fields to update>"}}

'fields' is specific to fields that are being explicitly set to a certain value.
'update' is used to update the existing values of a field, such as adding or removing a value of an array field rather than overwriting it.

A more thorough explanation of this process can be found in the `jira documentation <https://developer.atlassian.com/server/jira/platform/jira-rest-api-examples/#editing-an-issue-examples>`_
but here is a quick example of appending to the components field:

.. code-block:: python

  {'update': {'components': [{'add': "my component"}]}}

Adding Comments
---------------

Adding comments is the same for all trackers. Set the 'add_comments' field with a list of strings and these will be
added as comments on the bug.
