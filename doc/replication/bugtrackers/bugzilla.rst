.. _bugzilla-api-doc:

Bugzilla Replication
====================
Handling of Bugzilla bugs is done via the `XML-RPC API <https://www.bugzilla.org/docs/4.4/en/html/api/Bugzilla/WebService/Bug.html>`_

The parameters to return from the replication script are the same as those defined by the Bugzilla API.


.. _create-bug-bz:

Creating Bugs
-------------
The fields to use when creating a bug are found in the Bugzilla documentation here: `create <https://www.bugzilla.org/docs/4.4/en/html/api/Bugzilla/WebService/Bug.html#create>`_

A dictionary is passed to the method which defines all the fields to set when creating the Bug. This dictionary is pulled
from the dictionary returned by the replication check method under the 'set_fields' key, e.g.

.. code-block:: python

  {'set_fields': {"<fields dictionary passed to create_issue>"}}

Pre-Populated Fields
^^^^^^^^^^^^^^^^^^^^
Handling of the url, bug id, and authentication are all handled by CI Bug Log based on the BugTracker configuration
in the database. These details don't need to be specified in the returned dictionary, just the parameters to set.


Updating Bugs
-------------
The fields used to update bugs are found in the Bugzilla documentation here: `update <https://www.bugzilla.org/docs/4.4/en/html/api/Bugzilla/WebService/Bug.html#update>`_

The format of the dict is the same as described in :ref:`create-bug-bz`


Adding Comments
---------------

Adding comments is the same for all trackers. Set the 'add_comments' field with a list of strings and these will be
added as comments on the bug.
